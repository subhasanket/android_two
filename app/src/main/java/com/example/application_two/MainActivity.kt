package com.example.application_two

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.viewpager.widget.PagerAdapter
import kotlinx.android.synthetic.main.activity_main.*

var imageIndex = 0
private val image = arrayOf(R.drawable.ic_flag_of_argentina,R.drawable.ic_flag_of_australia,R.drawable.ic_flag_of_belgium,R.drawable.ic_flag_of_brazil,
    R.drawable.ic_flag_of_denmark,R.drawable.ic_flag_of_fiji,R.drawable.ic_flag_of_germany,R.drawable.ic_flag_of_india,R.drawable.ic_flag_of_kuwait,R.drawable.ic_flag_of_new_zealand)
class MainActivity : AppCompatActivity()  {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toggle_button.setOnCheckedChangeListener{_, isChecked ->
            if (isChecked){
                setCurrentImage()
                Toast.makeText(this,"Slide show is ON",Toast.LENGTH_LONG).show()

            }
            else{
                setCurrentImage()
                Toast.makeText(this,"Slide show is OFF",Toast.LENGTH_LONG).show()
            }
        }

        btn_prev.setOnClickListener {
            if (imageIndex == 0){
                imageIndex = image.size-1
            }
            --imageIndex
            setCurrentImage()
        }
        btn_next.setOnClickListener {
            if (imageIndex == image.size-1)
            {
                imageIndex = 0
            }
            ++imageIndex
            setCurrentImage()
        }

    }

    private fun setCurrentImage(){
        image_view.setImageResource(image[imageIndex])
    }
    private  fun slideshow(){
        if (imageIndex == image.size-1)
        {
            imageIndex = 0
        }
        ++imageIndex
        setCurrentImage()
    }


}

